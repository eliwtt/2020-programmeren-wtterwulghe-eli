﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeginnenMetCSharp2
{
    class Hoofdstuk3B
    {
        static public void StringInterpolation()
        {
            int getal = 411;

            Console.WriteLine($"1 * {getal} is  {getal}");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"2 * {getal} is {getal * 2}");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"3 * {getal} is {getal * 3}");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"4 * {getal} is {getal * 4}");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"5 * {getal} is {getal * 5}");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"6 * {getal} is {getal * 6}");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"7 * {getal} is {getal * 7}");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"8 * {getal} is {getal * 8}");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"9 * {getal} is {getal * 9}");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"10 * {getal} is {getal * 10}");
            Console.ReadLine();
            Console.Clear();

            double gewicht = 100;
            const double Mercurius = 0.38;
            const double Venus = 0.91;
            const double Aarde = 1.00;
            const double Mars = 0.38;
            const double Jupiter = 2.34;
            const double Saturnus = 1.06;
            const double Uranus = 0.92;
            const double Neptunus = 1.19;
            const double Pluto = 0.06;

            Console.WriteLine($"Op Mercurius voel je je alsof je {gewicht * Mercurius} kg weegt.");
            Console.WriteLine($"Op Mercurius voel je je alsof je {gewicht * Venus} kg weegt.");
            Console.WriteLine($"Op Mercurius voel je je alsof je {gewicht * Aarde} kg weegt.");
            Console.WriteLine($"Op Mercurius voel je je alsof je {gewicht * Mars} kg weegt.");
            Console.WriteLine($"Op Mercurius voel je je alsof je {gewicht * Jupiter} kg weegt.");
            Console.WriteLine($"Op Mercurius voel je je alsof je {gewicht * Saturnus} kg weegt.");
            Console.WriteLine($"Op Mercurius voel je je alsof je {gewicht * Uranus} kg weegt.");
            Console.WriteLine($"Op Mercurius voel je je alsof je {gewicht * Neptunus} kg weegt.");
            Console.WriteLine($"Op Mercurius voel je je alsof je {gewicht * Pluto} kg weegt.");
        }

        static public void BerekenBtw()
        {
            Console.Write($"Geef het bedrag in: ");
            int bedrag = Convert.ToInt32(Console.ReadLine());
            Console.Write($"Geef BTW percentage in: ");
            int percentage = Convert.ToInt32(Console.ReadLine());
            float result = bedrag + bedrag / 100f * percentage;
            Console.Write($"Het bedrag {bedrag} met {percentage}% btw bedraagt {result:F2}");
        }

        static public void LeetSpeak()
        {
            Console.WriteLine("Geef je tekst in");
            string tekst = Console.ReadLine();
            string result = tekst.Replace("a", "@").Replace(" ", "");
            Console.WriteLine(result);
        }

        static public void Instructies()
        {
            Console.WriteLine("Wat is je naam?");
            string naam = Console.ReadLine();
            string letters = naam.Substring(0, 3);
            string hoofdletters = letters.ToUpper();

            Console.WriteLine("Wat is de naam van de cursus?");
            string cursus = Console.ReadLine();

            Console.Write($@"Maak een map als volgt: \{hoofdletters}\{cursus}");
        }

        static public void Lotto()
        {
            Console.WriteLine("Wat zijn je getallen (tussen 01 en 45)?");
            string getallen = Console.ReadLine();
            getallen = getallen.Replace(",", "\t");
            Console.WriteLine($"Je cijfers zijn:\n{getallen.Substring(0, 8)}\n{getallen.Substring(9)}");
        }

        static public void SuperLotto()
        {
            Console.WriteLine("Wat zijn je getallen (tussen 01 en 45)?");
            string Lotto = Console.ReadLine();

            int cijfer1 = Convert.ToInt32(Lotto.Substring(0, Lotto.IndexOf(",")));
            int index = Lotto.IndexOf(",");
            Lotto = Lotto.Substring(index + 1);

            int cijfer2 = Convert.ToInt32(Lotto.Substring(0, Lotto.IndexOf(",")));
            index = Lotto.IndexOf(",");
            Lotto = Lotto.Substring(index + 1);

            int cijfer3 = Convert.ToInt32(Lotto.Substring(0, Lotto.IndexOf(",")));
            index = Lotto.IndexOf(",");
            Lotto = Lotto.Substring(index + 1);

            int cijfer4 = Convert.ToInt32(Lotto.Substring(0, Lotto.IndexOf(",")));
            index = Lotto.IndexOf(",");
            Lotto = Lotto.Substring(index + 1);

            int cijfer5 = Convert.ToInt32(Lotto.Substring(0, Lotto.IndexOf(",")));
            index = Lotto.IndexOf(",");
            Lotto = Lotto.Substring(index + 1);

            int cijfer6 = Convert.ToInt32(Lotto.Substring(0));
            Console.WriteLine($"Je cijfer zijn:\n{cijfer1:d2}\t{cijfer2:d2}\t{cijfer3:d2}\n{cijfer4:d2}\t{cijfer5:d2}\t{cijfer6:d2}");
        }


    }
    
    
}
