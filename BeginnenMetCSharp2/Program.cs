﻿using System;

namespace BeginnenMetCSharp2
{
    class Program
    {
        static void Main(string[] args)
        {
            SayHello();
            // EenProgrammeSchrijvenInCSharp.ZegGoedenDag();
            // EenProgrammeSchrijvenInCSharp.MijnEersteProgramma();
            //EenProgrammeSchrijvenInCSharp.Rommelzin();

            // Hoofdstuk2B.VariabelenHoofdletters();
            //Hoofdstuk2B.optellen();
            // Hoofdstuk2B.verbruikwagen();
            // Hoofdstuk2B.BeetjeWiskunde();
            //Hoofdstuk2B.Gemiddelde();
            //Hoofdstuk2B.Maaltafels();
            //Hoofdstuk2B.ruimte();
            //Hoofdstuk3B.StringInterpolation();
            //Hoofdstuk3B.BerekenBtw();
            //Hoofdstuk3B.LeetSpeak();
            //Hoofdstuk3B.Instructies();
            //Hoofdstuk3B.Lotto();
            Hoofdstuk3B.SuperLotto();


        }

        static void SayHello()
        {
            Console.WriteLine("Hello World!");
        }
    }
}
