﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace BeginnenMetCSharp2
{
    class Hoofdstuk2B
    {
        static public void VariabelenHoofdletters()
        {
            Console.Write("Wat is je naam? ");
            string naam = Console.ReadLine();
            Console.WriteLine("Dus ja naam is: " + naam.ToUpper());
        }
        static public void optellen()
        {
            Console.WriteLine("Wat is het eerste getal?");
            int getal1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Wat is het tweede getal?");
            int getal2 = Convert.ToInt32(Console.ReadLine());

            int som = getal1 + getal2;

            Console.WriteLine("De som is: " + som);
        }
        static public void verbruikwagen()
        {
            Console.WriteLine("Geef het aantal liter in de tank voor de rit:");
            double liter1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Geef het aantal liter na de rit:");
            double liter2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Geef kilometerstand1 van je auto voor de rit:");
            double kilomertstand1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Geef kilometerstand2 van je auto na de rit:");
            double kilomertstand2 = Convert.ToDouble(Console.ReadLine());

            double a = (100 * (liter1 - liter2) / (kilomertstand2 - kilomertstand1));
            Math.Round(a, 2);

            Console.WriteLine("het verbuik van de auto is: " + a);
            Console.WriteLine("Het afgeronde verbruik van de auto is: " + Math.Round(a, 2));

        }
        static public void BeetjeWiskunde()
        {
            int a = -1 + 4 * 6;
            int b = (35 + 5) % 7;
            float c = 14 + -4 * 6 / 11;
            float d = 2 + 15 / 6 * 1 - 7 % 2;

            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine(d);



        }
        static public void Gemiddelde()
        {
            int a = 18;
            int b = 11;
            int c = 8;
            int d = 3;

            float f = (a + b + c) / d;
            Console.Write(f);


        }
        static public void Maaltafels()
        {
            int a = 411;
            int b = 1;
            int c = 2;
            int d = 3;
            int e = 4;
            int f = 5;
            int g = 6;
            int h = 7;
            int i = 8;
            int j = 9;
            int k = 10;

            int result = b * a;

            Console.WriteLine(b +  " * "  +  a + " is " + result);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result1 = c * a;
            Console.WriteLine(c + " * " + a + " is " + result1);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result2 = d * a;
            Console.WriteLine(d + " * " + a + " is " + result2);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result3 = e * a;
            Console.WriteLine(e + " * " + a + " is " + result3);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result4 = f * a;
            Console.WriteLine(f + " * " + a + " is " + result4);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result5 = g * a;
            Console.WriteLine(g + " * " + a + " is " + result5);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result6 = h * a;
            Console.WriteLine(h + " * " + a + " is " + result6);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result7 = i * a;
            Console.WriteLine(i + " * " + a + " is " + result7);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result8 = j * a;
            Console.WriteLine(j + " * " + a + " is " + result8);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();

            int result9 = k * a;
            Console.WriteLine(k + " * " + a + " is " + result9);
            Console.Write(" ");
            Console.ReadLine();
            Console.Clear();
        }

        static public void ruimte()
        {
            double gewichtOpAarde = 69.0;        
            double zwaartekrachtAarde = 1.0;    
            double zwaartekrachtMercurius = 0.38;    
            double zwaartekrachtVenus = 0.91;
            double zwaartekrachtMars = 0.38;
            double zwaartekrachtJupiter = 2.34;
            double zwaartekrachtSaturnus = 1.06;
            double zwaartekrachtUranus = 0.92;
            double zwaartekrachtNeptunus = 1.19;
            double zwaartekrachtPluto = 0.06;


            double gewichtOpMercurius = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtMercurius;
            Console.WriteLine("Op Mercurius voel je je alsof je" + gewichtOpMercurius + " kg weegt");

            double gewichtOpVenus = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtVenus; 
            Console.WriteLine("Op Venus voel je je alsof je" + gewichtOpVenus + " kg weegt");

            double gewichtOpMars = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtMars; 
            Console.WriteLine("Op Mars voel je je alsof je" + gewichtOpMars + " kg weegt");

            double gewichtOpJupiter = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtJupiter;
            Console.WriteLine("Op Jupiter voel je je alsof je" + Math.Round(gewichtOpJupiter,2) + " kg weegt");

            double gewichtOpSaturnus = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtSaturnus;
            Console.WriteLine("Op Saturnus voel je je alsof je" + gewichtOpSaturnus + " kg weegt");

            double gewichtOpUranus = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtUranus; 
            Console.WriteLine("Op Uranus voel je je alsof je" + Math.Round(gewichtOpJupiter, 2) + " kg weegt");

            double gewichtOpNeptunus = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtNeptunus;
            Console.WriteLine("Op Neptunus voel je je alsof je" + gewichtOpNeptunus + " kg weegt");

            double gewichtOpPluto = (gewichtOpAarde / zwaartekrachtAarde) * zwaartekrachtPluto;
            Console.WriteLine("Op Pluto voel je je alsof je" + gewichtOpPluto + " kg weegt");
        }
    }

}
